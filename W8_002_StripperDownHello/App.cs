﻿using System;
using System.Linq;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace W8_002_StripperDownHello
{
    public class App : Application
    {
        static void Main(string[] args)
        {
            Application.Start(p => new App());
        }

        protected override void OnLaunched(Windows.ApplicationModel.Activation.LaunchActivatedEventArgs args)
        {
            TextBlock tb = new TextBlock
            {
                Text = "Stripped down window8 examples",
                FontFamily = new Windows.UI.Xaml.Media.FontFamily("Lucida sans Typewriter"),
                FontSize = 96,
                Foreground = new SolidColorBrush(Colors.Red),
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center
            };

            Window.Current.Content = tb;
            Window.Current.Activate();
        }
    }
}
