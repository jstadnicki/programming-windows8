﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace W8_012_RadioButtonsTags
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class BlankPage : Page
    {
        public BlankPage()
        {
            this.InitializeComponent();
            foreach (UIElement child in startLineCapPanel.Children)
                (child as RadioButton).IsChecked = (PenLineCap)(child as RadioButton).Tag == polyline.StrokeStartLineCap;

            foreach (UIElement child in endLineCapPanel.Children)
                (child as RadioButton).IsChecked = (PenLineCap)(child as RadioButton).Tag == polyline.StrokeStartLineCap;

            foreach (UIElement child in lineJoinPanel.Children)
                (child as RadioButton).IsChecked = (PenLineJoin)(child as RadioButton).Tag == polyline.StrokeLineJoin;
        }

        void OnStartLineCapRadioButtonChecked(object sender, RoutedEventArgs args)
        {
            polyline.StrokeStartLineCap = (PenLineCap)(sender as RadioButton).Tag;
        }

        void OnEndLineCapRadioButtonChecked(object sender, RoutedEventArgs args)
        {
            polyline.StrokeEndLineCap = (PenLineCap)(sender as RadioButton).Tag;
        }

        void OnLineJoinRadioButtonChecked(object sender, RoutedEventArgs args)
        {
            polyline.StrokeLineJoin = (PenLineJoin)(sender as RadioButton).Tag;
        }
    }
}