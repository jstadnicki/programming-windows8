﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace W8_016_ApplicationBar
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }   

        private void OnWrapOptionsBarButtonClick(object s, RoutedEventArgs a)
        {
            WrapDialog wd = new WrapDialog
            {
                TextWrapping = this.textblock.TextWrapping
            };

            Binding b = new Binding
            {
                Source = wd,
                Path = new PropertyPath("TextWrapping"),
                Mode = BindingMode.TwoWay
            };

            this.textblock.SetBinding(TextBox.TextWrappingProperty, b);

            Popup p = new Popup
            {
                Child = wd,
                IsLightDismissEnabled = true
            };

            wd.SizeChanged +=(_,e)=>
                {
                    p.VerticalOffset = this.ActualHeight - wd.ActualHeight - this.BottomAppBar.ActualHeight-48;
                    p.HorizontalOffset=48;
                };

            p.IsOpen=true;




        }
        
        private void OnSaveAsAppBarButtonClick(object s, RoutedEventArgs a)
        { 
        }

        private void OnOpenAppBarButtonClick(object s, RoutedEventArgs a)
        {
        }
    }
}
