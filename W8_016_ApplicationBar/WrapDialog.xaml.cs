﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace W8_016_ApplicationBar
{
    public sealed partial class WrapDialog : UserControl
    {
        static WrapDialog()
        {
            TextWrappingProperty = 
                DependencyProperty.Register("TextWrapping", 
                typeof(TextWrapping), 
                typeof(WrapDialog), 
                new PropertyMetadata(TextWrapping.NoWrap, OnTextWrappingChanged));

        }

        public WrapDialog()
        {
            this.InitializeComponent();

        }


        public static readonly DependencyProperty TextWrappingProperty;

        public TextWrapping TextWrapping
        {
            get { return (TextWrapping)this.GetValue(TextWrappingProperty); }
            set { this.SetValue(TextWrappingProperty, value); }
        }

        static void OnTextWrappingChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as WrapDialog).OnTextWrappingChanged(e);
        }

        private void OnTextWrappingChanged(DependencyPropertyChangedEventArgs args)
        {
            foreach (UIElement child in this.stackpanel.Children)
            {
                RadioButton rb = child as RadioButton;
                rb.IsChecked = (TextWrapping)rb.Tag == (TextWrapping)args.NewValue;
            }
        }

        private void OnRadioButtonChecked(object sender, RoutedEventArgs e)
        {
            this.TextWrapping = (TextWrapping)(sender as RadioButton).Tag;
        }
    }
}