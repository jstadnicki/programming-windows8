using System;

namespace W8_015_BindableBase
{
    public class SimplyViewModel : BindableBase
    {
        string text;

        public String Text
        {
            get
            {
                return this.text;
            }
            set
            {
                //this.SetProperty<string>(ref this.text, value);
                this.text = value;
                this.OnPropertyChanged2();
            }
        }
    }
}