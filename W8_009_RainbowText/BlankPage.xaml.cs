﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace W8_009_RainbowText
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class BlankPage : Page
    {
        private double textSize;
        public BlankPage()
        {
            this.InitializeComponent();
            this.Loaded += BlankPage_Loaded;
        }

        void BlankPage_Loaded(object sender, RoutedEventArgs e)
        {
            this.textSize = this.textblock.ActualHeight;
            CompositionTarget.Rendering += CompositionTarget_Rendering;
        }

        void CompositionTarget_Rendering(object sender, object e)
        {
            this.textblock.FontSize = this.ActualHeight / this.textSize;

            RenderingEventArgs rargs = e as RenderingEventArgs;
            double t = (0.25 * rargs.RenderingTime.TotalSeconds) % 1;

            for (int i = 0; i < this.gradientbrush.GradientStops.Count; i++)
            {
                this.gradientbrush.GradientStops[i].Offset = i / 7.0 - t;
            }
        }



    }
}
