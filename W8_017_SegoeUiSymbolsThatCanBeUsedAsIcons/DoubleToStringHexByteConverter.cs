﻿using System;
using System.Linq;
using Windows.UI.Xaml.Data;

namespace W8_017_SegoeUiSymbolsThatCanBeUsedAsIcons
{
    public class DoubleToStringHexByteConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return ((int)(double)value).ToString("X2") + "__";
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
    }
}
