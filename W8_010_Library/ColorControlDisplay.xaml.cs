﻿using System;
using System.Linq;
using Windows.UI;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace W8_010_Library
{
    public sealed partial class ColorControlDisplay : UserControl
    {
        public ColorControlDisplay(string name, Color clr)
        {
            this.InitializeComponent();

            this.rectangle.Fill = new SolidColorBrush(clr);
            this.txtblkName.Text = name;
            this.txtblkRgb.Text = String.Format("{0:X2}-{1:X2}-{2:X2}-{3:X2}", clr.A, clr.R, clr.G, clr.B);
        }
    }
}
