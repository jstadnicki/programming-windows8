﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace W8_007_OverridingHandledEvent
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class BlankPage : Page
    {
        public BlankPage()
        {
            this.InitializeComponent();
            //this.AddHandler(UIElement.TappedEvent, new TappedEventHandler(this.Page_Tapped), true);
            //this.AddHandler(UIElement.TappedEvent, new TappedEventHandler(this.Page_Tapped), false);
            this.Tapped += this.Page_Tapped;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void TB_Tapped(object sender, TappedRoutedEventArgs e)
        {
            (sender as TextBlock).Text = DateTime.Now.TimeOfDay.ToString();
            e.Handled = true;
        }

        private void Page_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.TL.Text = "Top-Left";
            this.TC.Text = "Top-Center";
            this.TR.Text = "Top-Right";

            this.CL.Text = "Center-Left";
            this.CC.Text = "Center-Center";
            this.CR.Text = "Center-Right";

            this.BL.Text = "Botton-Left";
            this.BC.Text = "Botton-Center";
            this.BR.Text = "Bottom-Right";

        }
    }
}
